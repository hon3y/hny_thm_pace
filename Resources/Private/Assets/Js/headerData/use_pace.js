;
(function() {
    if(hive_thm_pace_bUsePace) {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = ("https:" == document.location.protocol ? "https" : "http") + "://" + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + "/typo3conf/ext/hive_thm_pace/Resources/Public/Assets/Js/pace.min.js";
        a.type = "text/javascript";
        a.async = "true";
        b.parentNode.insertBefore(a, b)
    }
})();